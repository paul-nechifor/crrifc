#ifndef _CULORI_H
#define _CULORI_H

/* indici de culori */
#define COL_GRANITA 1
#define COL_FUNDAL 2
#define COL_NECUCERIT 3
#define COL_J1 4
#define COL_J2 5
#define COL_J3 6
#define COL_MESAJ_SERVER 7
#define COL_STATUS 8
#define COL_FUNDAL_INTREBARE 9
#define COL_INTREBARE 10
#define COL_RASPUNS 11

void initializeazaCulori(void);

#endif //_CULORI_H
