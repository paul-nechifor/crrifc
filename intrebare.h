/**
 * alege o intrebare rapida la intamplare si o copiaza in 'text'.
 * Raspunsul este pus in 'corect'.
 */
void getIntrebareRapida(char text[200], int* corect);
/**
 * Alege o intrebare grila la intamplare si o copiaza in 'text'. 
 * Cele patru raspunsuri posibile sunt puse in 'ras' si indicele 
 * celei corecte este pus in 'corect'.
 */
void getIntrebareGrila(char text[200], char ras[4][100], int* corect);
